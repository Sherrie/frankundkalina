package de.htw.cbir.frankundkalina;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

import de.htw.cbir.feature.FeatureFactory;
import de.htw.cbir.model.Pic;
import de.htw.cbir.model.Settings;
import de.htw.cbir.model.Vector3;

public class TextureElementsCombined extends FeatureFactory {

	private final int THUMBNAIL_WIDTH = 8;
	private final int THUMBNAIL_HEIGHT = 8;

	private final int numberOfHue = 8;
	private final int numberOfSat = 3;
	private final int numberOfVal = 3;

	private final float horizontalCrop = 1f;
	private final float verticalCrop = 1f;

	private boolean REDUCE_HUE = true;

	public TextureElementsCombined(Settings settings) {
		super(settings);
	}

	///////////////////////////////////////////
	// visualize the feature data as image
	//
	@Override
	public BufferedImage getFeatureImage(Pic image) {

		int w = 16 * (numberOfHue + numberOfSat + numberOfVal);
		int h = 60;

		BufferedImage bi = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
		Graphics2D big = bi.createGraphics();
		int[] pixels = new int[h * w];
		float[] featureVector = image.getFeatureVector();

		// for (int i = 0; i < featureVector.length; i++) {
		// System.out.print(featureVector[i] + ", ");
		// }

		float highestValue = 0;
		for (int i = 0; i < featureVector.length; i++) {
			if (featureVector[i] > highestValue)
				highestValue = featureVector[i];
		}
		float valueToMod = 1f / highestValue;

		for (int x = 0; x < w; x++) {

			Color color = Color.WHITE; // Color.HSBtoRGB(featureVector[x],
										// featureVector[x+1],
										// featureVector[x+2]);

			for (int y = 0; y < h; y++) {

				int pos = y * w + x;
				if (h - valueToMod * featureVector[x] * h <= y) {
					pixels[pos] = (0xFF << 24) | (color.getRed() << 16) | (color.getGreen() << 8) | color.getBlue();
				} else {
					pixels[pos] = (0xFF << 24) | (0 << 16) | (0 << 8) | 0;
				}
			}
		}

		BufferedImage bThumb = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
		bThumb.setRGB(0, 0, w, h, pixels, 0, w);

		big.drawImage(bThumb, 0, 0, w, h, null);
		big.dispose();
		return bi;

	}

	@Override
	public float[] getFeatureVector(Pic image) {
		BufferedImage bCrop = getCroppedImage(image);

		float[] quantifiedColorValues = getQuantifiedValues(bCrop);

		return getStatisticalHistogram(
				getQuantifiedField(quantifiedColorValues, (int) THUMBNAIL_WIDTH, (int) THUMBNAIL_HEIGHT));
	}

	float[] getQuantifiedValues(BufferedImage bi) {
		int width = bi.getWidth();
		int height = bi.getHeight();

		int[] rgbValues = new int[width * height];
		bi.getRGB(0, 0, width, height, rgbValues, 0, width);

		int blockWidth = width / THUMBNAIL_WIDTH;
		int blockHeight = height / THUMBNAIL_HEIGHT;

		float[] quantifiedColorValues = new float[3 * THUMBNAIL_WIDTH * THUMBNAIL_HEIGHT];

		int r, g, b, sum;

		for (int tX = 0; tX < THUMBNAIL_WIDTH; tX++) {
			for (int tY = 0; tY < THUMBNAIL_HEIGHT; tY++) {
				// loop over the block
				r = 0;
				g = 0;
				b = 0;
				sum = 0;

				// PIXEL LOOPS
				for (int x = 0; x < blockWidth; x++) {

					for (int y = 0; y < blockHeight; y++) {

						int pos = (width * tY * blockHeight + y * width) + (tX * blockWidth + x);
						r += (rgbValues[pos] >> 16) & 255;
						g += (rgbValues[pos] >> 8) & 255;
						b += (rgbValues[pos]) & 255;
						sum++;

					}

				}

				float[] hsv = new float[3];
				Color.RGBtoHSB(r / sum, g / sum, b / sum, hsv);

				float hVal = 360 * hsv[0];
				int h = 0;
				int s = 0;
				int v = 0;

				if (hVal <= 24 || hVal >= 345) {
					h = 0;
				} else if (hVal <= 49) {
					h = 1;
				} else if (hVal <= 79) {
					h = 2;
				} else if (hVal <= 159) {
					h = 3;
				} else if (hVal <= 194) {
					h = 4;
				} else if (hVal <= 264) {
					h = 5;
				} else if (hVal <= 284) {
					h = 6;
				} else {
					h = 7;
				}

				if (hsv[1] <= 0.15f) {
					s = 0;
				} else if (hsv[1] <= 0.8f) {
					s = 1;
				} else {
					s = 2;
				}

				if (hsv[2] <= 0.15f) {
					v = 0;
				} else if (hsv[2] <= 0.8f) {
					v = 1;
				} else {
					v = 2;
				}

				// compute the mean color
				int thumbPos = 3 * (tY * THUMBNAIL_WIDTH + tX);

				quantifiedColorValues[thumbPos] = h;
				quantifiedColorValues[thumbPos + 1] = s;
				quantifiedColorValues[thumbPos + 2] = v;
			}
		}

		return quantifiedColorValues;
	}

	float[][] getQuantifiedField(float[] values, int width, int height) {
		float[][] returnValues = new float[width * 3][height];

		int x = 0;
		int y = 0;

		for (int i = 0; i < values.length; i++) {
			if (i - x >= width * 3) {
				x += width * 3;
				y++;
			}

			returnValues[i - x][y] = values[i];
		}

		return returnValues;
	}

	// 5 7
	// 5 3

	// 5

	// 1 0
	// 1 0
	private int getClusterValue(Vector3 topLeftValue, Vector3 topRightValue, Vector3 bottomLeftValue,
			Vector3 bottomRightValue, Vector3 checkValue) {
		boolean tl = topLeftValue.equals(checkValue);
		boolean tr = topRightValue.equals(checkValue);
		boolean bl = bottomLeftValue.equals(checkValue);
		boolean br = bottomRightValue.equals(checkValue);

		int returnValue = 0;

		// br bl tr tl
		// 1 0 1 0

		if (tl)
			returnValue += 1;
		if (tr)
			returnValue += 2;
		if (bl)
			returnValue += 4;
		if (br)
			returnValue += 8;

		return returnValue;
	}

	private float[] getStatisticalHistogram(float[][] quantizedImage) {

		float[] featureVector = new float[(int) (16 * numberOfHue * numberOfSat * numberOfVal)];

		// Color1 (0 0 0): 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15
		// Color2 (0 0 1): 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15
		// Color3 (0 0 2): 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15
		// Color4 (0 1 0): 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15
		// ...

		for (int y = 0; y < quantizedImage[1].length; y += 2) {

			for (int x = 0; x < quantizedImage[0].length; x += 6) {

				int counter = 0;
				for (int h = 0; h < numberOfHue; h++) {
					for (int s = 0; s < numberOfSat; s++) {
						for (int v = 0; v < numberOfVal; v++) {
							int textureElementIndex = getClusterValue(
									new Vector3(quantizedImage[x][y], quantizedImage[x + 1][y],
											quantizedImage[x + 2][y]),
									new Vector3(quantizedImage[x + 3][y], quantizedImage[x + 4][y],
											quantizedImage[x + 5][y]),
									new Vector3(quantizedImage[x][y + 1], quantizedImage[x + 1][y + 1],
											quantizedImage[x + 2][y + 1]),
									new Vector3(quantizedImage[x + 3][y + 1], quantizedImage[x + 4][y + 1],
											quantizedImage[x + 5][y + 1]),
									new Vector3(h, s, v));
							featureVector[16 * counter + textureElementIndex]++;
							counter++;
						}
					}
				}
			}

		}

		// normalize
		int sum = 0;
		for (int i = 0; i < featureVector.length; i++) {
			sum += featureVector[i];
		}
		for (int i = 0; i < featureVector.length; i++) {
			featureVector[i] = featureVector[i] / sum;
		}

		return featureVector;
	}

	@Override
	public float getDistance(float[] fv1, float[] fv2) {
		return getL2Distance(fv1, fv2);
	}

	@Override
	public String getName() {
		return "TextureElementsCombined";
	}

	float getHue(int index) {

		float angle = 0;

		switch (index) {
		case 0:
			angle = 5f;
			break;
		case 1:
			angle = 36f;
			break;
		case 2:
			angle = 65f;
			break;
		case 3:
			angle = 120f;
			break;
		case 4:
			angle = 177f;
			break;
		case 5:
			angle = 230f;
			break;
		case 6:
			angle = 275f;
			break;
		case 7:
			angle = 315f;
			break;
		}

		return angle / 360f;
	}

	float getSaturation(int index) {

		return (float) ((float) 1f / (float) numberOfSat * (float) index);
	}

	float getValue(int index) {

		return (float) ((float) 1f / (float) numberOfVal * (float) index);
	}

	private BufferedImage getCroppedImage(Pic image) {
		BufferedImage buffImg = image.getDisplayImage();

		int w = buffImg.getWidth();
		int h = buffImg.getHeight();

		int cW = (int) (w * horizontalCrop);
		if (cW % 2 == 1 && (cW != w))
			cW--;
		int cH = (int) (h * verticalCrop);
		if (cH % 2 == 1 && (cH != h))
			cH--;

		BufferedImage bCrop = new BufferedImage(cW, cH, BufferedImage.TYPE_INT_ARGB);

		int[] rgbValues = new int[cW * cH];

		buffImg.getRGB((w - cW) / 2, (h - cH) / 2, cW, cH, rgbValues, 0, cW);

		bCrop.setRGB(0, 0, cW, cH, rgbValues, 0, cW);

		return bCrop;
	}

	public BufferedImage getQuantizedImage(Pic image) {

		BufferedImage bCrop = getCroppedImage(image);

		float[] quantifiedValues = getQuantifiedValues(bCrop);

		int h = 0;
		int s = 0;
		int v = 0;

		int[] pixels = new int[THUMBNAIL_WIDTH * THUMBNAIL_HEIGHT];

		int rVal = 0;
		int gVal = 0;
		int bVal = 0;

		for (int x = 0; x < THUMBNAIL_WIDTH * 3; x += 3) {
			for (int y = 0; y < THUMBNAIL_HEIGHT; y++) {
				int pos = y * (THUMBNAIL_WIDTH * 3) + x;
				int quantPos = y * THUMBNAIL_WIDTH + (x / 3);

				h = (int) quantifiedValues[pos];
				s = (int) quantifiedValues[pos + 1];
				v = (int) quantifiedValues[pos + 2];

				int rgb = Color.HSBtoRGB(getHue(h), getSaturation(s), getValue(v));

				rVal = (rgb >> 16) & 255;
				gVal = (rgb >> 8) & 255;
				bVal = (rgb) & 255;

				pixels[quantPos] = (0xFF << 24) | (rVal << 16) | (gVal << 8) | bVal;
			}

		}

		BufferedImage bi = new BufferedImage(THUMBNAIL_WIDTH, THUMBNAIL_HEIGHT, BufferedImage.TYPE_INT_ARGB);
		Graphics2D big = bi.createGraphics();

		BufferedImage bThumb = new BufferedImage(THUMBNAIL_WIDTH, THUMBNAIL_HEIGHT, BufferedImage.TYPE_INT_ARGB);
		bThumb.setRGB(0, 0, THUMBNAIL_WIDTH, THUMBNAIL_HEIGHT, pixels, 0, THUMBNAIL_WIDTH);

		big.drawImage(bThumb, 0, 0, THUMBNAIL_WIDTH, THUMBNAIL_HEIGHT, null);
		big.dispose();
		return bi;
		// return bCrop;
	}
}
